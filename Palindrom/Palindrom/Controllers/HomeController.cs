﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Palindrom.Models;

namespace Palindrom.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            return View("");
        }

        // POST: 
        [HttpPost]
        public ActionResult Index(string eingabe)
        {
            Palindromtest pt = new Palindromtest();
            bool check = pt.PTest(eingabe);
            string ergebnis = check ? "ein" : "kein";

            return View(model : ergebnis);
        }
    }
}