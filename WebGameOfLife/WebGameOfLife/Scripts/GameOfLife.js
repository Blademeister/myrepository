﻿$(document).ready(function () {
    var canvas = document.getElementById('game-of-life-board');
    var context = canvas.getContext('2d');
    var canvasSize = 500;
    canvas.width = canvasSize;
    canvas.height = canvasSize / 2;

    var positionen = [];
    for (var x = 0; x < canvas.width / 5; x++)
    {
        for (var y = 0; y < canvas.height / 5; y++)
        {
            positionen[y * canvas.width / 5 + x] = false;
        }
    }

    feldZeichnen(canvas, context);

    var zufallBtn = document.getElementById('zufall');
    zufallBtn.onclick = function () {
        feldZufälligFüllen(canvas, context, positionen);
    }

    var neuBtn = document.getElementById('neu');
    neuBtn.onclick = function () {
        clearFeld(canvas, context);
    }

    function clearFeld(canvas, context) {
        feldZeichnen(canvas, context);
    }

    var startBtn = document.getElementById('start');
    startBtn.onclick = function () {
        startGame(canvas, context, positionen, startBtn);
    }

    function startGame(canvas, context, positionen, startBtn) {
        start(canvas, context, positionen, startBtn);
    }

    addEventListener('click', function (event) {
        var rect = canvas.getBoundingClientRect();

        var x = event.clientX - rect.left;
//        alert(x);
        var y = event.clientY - rect.top;
//        alert(y);
        var xdiff = x % 5;
        var ydiff = y % 5;
        if (xdiff > 0 && ydiff > 0) {
            fillPlace(x, y, "red", context, positionen);
        }
    }, false);
});


function workerNachrichtVerarbeiten(event) {
    "use strict";

    var daten = event.data;

    // Empfangene Daten verarbeiten
    var typ = daten.split(":")[0];
    var inhalt = daten.split(":")[1];

    switch (typ) {
        case "prozent":
            // Prozentwert aus dem Worker in das Textfeld schreiben        
            document.getElementById("prozent").innerHTML = inhalt + "%";

            // Progress aktualisieren
            document.getElementById("progress").value = inhalt;

            break;

        case "ergebnis":
            // Endergebnis aus dem Worker in das Textfeld schreiben
            document.getElementById("ergebnis").value = inhalt;

            // Start Button für erneutes anklicken aktivieren
            document.getElementById("start").disabled = false;

            break;
    }
}

function feldZufälligFüllen(canvas, context, positionen) {
    feldZeichnen(canvas, context);
    for (var n = 1; n <= canvas.width * canvas.height / 25 / 3; n++) {
        setLife(canvas, context, positionen);
    }
}

function fillPlace(x, y, color, context, positionen) {
    var xdiff = x % 5;
    var ydiff = y % 5;
    if (xdiff > 0 && ydiff > 0) {
        context.fillStyle = color;
        context.fillRect(x - xdiff + 1, y - ydiff + 1, 3, 3);
        var index = (Math.floor(y / 5) * 100) + Math.floor(x / 5);
        positionen[index] = true;
        // alert(index);
    }
}


function feldZeichnen(canvas, context) {
    'use strict';

    // Feld zeichnen:
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    for (var yPos = 0; yPos <= canvas.height; yPos += 5) {
        context.moveTo(0, yPos);
        context.lineTo(canvas.width, yPos);
        context.stroke();
    }
    context.beginPath();
    for (var xPos = 0; xPos <= canvas.width; xPos += 5) {
        context.beginPath();
        context.moveTo(xPos, 0);
        context.lineTo(xPos, canvas.height);
        context.stroke();
    }
}

function setLife(canvas, context, positionen) {
    'use strict';

    var rX = Math.round(Math.random(canvas.width) * canvas.width);
    var rY = Math.round(Math.random(canvas.height) * canvas.height);
    fillPlace(rX, rY, "red", context, positionen);
}

function lebendeNachbarnZählen(positionen, index)
{
    var keine_oben = false;
    var keine_unten = false;
    var keine_links = false;
    var keine_rechts = false;

    var lebendeNachbarn = 0;

    if (index < 100) {
        keine_oben = true;
    }
    if (index >= 4900)
    {
        keine_unten = true;
    }
    if (index % 100 == 0) {
        keine_links = true;
    }
    if (index % 100 == 99) {
        keine_rechts = true;
    }

    if (!(keine_oben || keine_links)) {
        lebendeNachbarn += (positionen[index - 100 - 1] === true) ? 1 : 0;
    }
    if (!keine_oben) {
        lebendeNachbarn += (positionen[index - 100] === true) ? 1 : 0;
    }
    if (!(keine_oben || keine_rechts)) {
        lebendeNachbarn += (positionen[index - 100 + 1] === true) ? 1 : 0;
    }
    if (!keine_links) {
        lebendeNachbarn += (positionen[index - 1] === true) ? 1 : 0;
    }
    if (!keine_rechts) {
        lebendeNachbarn += (positionen[index + 1] === true) ? 1 : 0;
    }
    if (!(keine_unten || keine_links)) {
        lebendeNachbarn += (positionen[index + 100 - 1] === true) ? 1 : 0;
    }
    if (!keine_unten) {
        lebendeNachbarn += (positionen[index + 100] === true) ? 1 : 0;
    }
    if (!(keine_unten || keine_rechts)) {
        lebendeNachbarn += (positionen[index + 100 + 1] === true) ? 1 : 0;
    }

    return lebendeNachbarn;
}   

function start(canvas, context, positionen, startBtn)
{
    // Start Button für erneutes klicken deaktivieren
    startBtn.disabled = true;

    // Web Worker anlegen
    var worker = new Worker("Webworker.js");

    worker.addEventListener("message", function (event) {
        console.log("Nachricht aus dem Worker: ", event.data);
    });
    
    // Bei Meldung aus dem Worker die Funktion workerNachrichtVerarbeiten() aufrufen
    worker.onmessage = workerNachrichtVerarbeiten;

    // Nachricht an Worker schicken und diesem damit "starten"
    worker.postMessage("");

    // Meldung in das Textfeld schreiben
    // document.getElementById("ergebnis").value = "Berechnung begonnen";
}