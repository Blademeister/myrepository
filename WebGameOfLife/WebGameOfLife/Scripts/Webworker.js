﻿window.onload = function () {
    self.addEventListener("message", function (event) {
        self.postMessage(event.data);
    });

    for (var index = 0; index < 5000; index++) {
        if (lebendeNachbarnZählen(positionen, index) === 3 && positionen[index] === false) {
            positionen[index] = true;
            var x = index % 100 + 1;
            var y = index / 100 + 1;
            console.log(index + " entsteht");
            fillPlace(x, y, "red", context, positionen);
        }

        if (positionen[index] === true && lebendeNachbarnZählen(positionen, index) <= 2) {
            positionen[index] = false;
            var x = index % 100 + 1;
            var y = index / 100 + 1;
            console.log(index + " stirbt");
            fillPlace(x, y, "white", context, positionen);
        }

        //if (lebendeNachbarnZählen(positionen, index) >= 2 && lebendeNachbarnZählen(positionen, index) <= 3 && positionen[index] === true) {
        //    positionen[index] = true;
        //    var x = index % 100 + 1;
        //    var y = index / 100 + 1;
        //    fillPlace(x, y, "white", context, positionen);
        //}

        if (lebendeNachbarnZählen(positionen, index) > 3 && positionen[index] === true) {
            positionen[index] = false;
            var x = index % 100 + 1;
            var y = index / 100 + 1;
            console.log(index + " stirbt");
            fillPlace(x, y, "white", context, positionen);
        }

        self.postMessage("");
    }
};